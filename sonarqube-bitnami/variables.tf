variable "namespace" {
  type    = string
}

variable "chart_version" {
  type    = string
}

variable "dns_name" {
  type    = string
}

variable "cluster_issuer_name" {
  type    = string
}

variable "subject_organizations" {
  type    = string
}

variable "subject_organizationalunits" {
  type    = string
}